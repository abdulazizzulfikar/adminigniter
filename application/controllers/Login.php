<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public $captcha_expired,$data_captcha;
	public function __construct(){
		parent::__construct();
		$this->load->model('AuthMdl');
		$this->load->model('GeneralMdl');
		$this->load->model('functions/CaptchaFunctions');
		$data = $this->CaptchaFunctions->captcha();
		$this->data_captcha = [
			'image'			=> $data['image'],
			'ip_address'	=> $this->input->ip_address(),
			'word'			=> $data['word'],
			'filename' 		=> $data['filename'],
			'time' 			=> $data['time']
		];
	}

	public function index(){

		$data_view = [
			'captcha_img'	=> $this->data_captcha['image']
		];

		if ($this->CaptchaFunctions->dbCaptchaManagement()) {
			$this->load->view('login',$data_view);
		}
		else{
			$err_status = [
				'err_code'		=> 'CAPTCHA_ERR',
				'err_status'	=> 'Captcha Tidak Bisa Dimuat',
				'err_detail'	=> 'Tidak Bisa Memuat Captcha, Silahkan Coba Refresh Halaman Ini Lagi'
			];
			$this->load->view('errors/html/error_custom_code', $err_status);
		}
	}

	public function authorize(){
		$config = array(
		        array(
		                'field' => 'username',
		                'label' => 'Username',
		                'rules' => 'trim|required|alpha_numeric'
		        ),
		        array(
		                'field' => 'password',
		                'label' => 'Password',
		                'rules' => 'trim|required'
		        ),
		        array(
		                'field' => 'captcha',
		                'label' => 'Captcha',
		                'rules' => 'trim|required|alpha_dash'
		        )	
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('status', validation_errors());
			redirect('Login');
		} else {
			$data = [
				'user'		=> $this->input->post('username'),
				'password'	=> $this->input->post('password')
			];
			$catpcha_validation = $this->CaptchaFunctions->captchaValidation($this->input->ip_address(),$this->input->post('captcha'));
			if(!$catpcha_validation){
				$this->session->set_flashdata('status', 'Captcha yang anda masukkan salah');
				redirect('Login');
			}
			elseif ($this->AuthMdl->checkCredentials($data) != TRUE) {
				$this->session->set_flashdata('status', 'user / password yang anda masukan salah');
				redirect('Login');
			}
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}

}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */