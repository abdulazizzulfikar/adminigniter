<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {

	private $userid,$usergroup,$view_data;
	public function __construct(){
		parent::__construct();

		$this->userid 		= $this->session->id_user; // USER ID
		$this->usergroup 	= 1; // TENTUKAN USER GROUP UNTUK CONTROLLER INI

		/*----------------------- CONFIG DEFAULT (JANGAN DIHAPUS)-----------------------*/
		$this->load->model('AuthMdl'); 			// MODEL DEFAULT YANG SELALU DI LOAD (UNTUK OTENTIKASI)
		$this->AuthMdl->auth($this->usergroup); // VALIDASI USER YANG LOGIN
		$this->load->model('GeneralMdl'); 		// MODEL DEFAULT YANG SELALU DI LOAD
		$this->view_data = $this->config_page();
		/*-----------------------------------------------------------------------------*/
		
		$this->load->model('admin/ConfigMdl'); 	// MODEL KHUSUS CONTROLLER INI
	}

	private function config_page(){
		$data = [
			// =============================================================================================
			// CONFIG PAGE DEFAULT
			// =============================================================================================
			'app_name'			=> $this->GeneralMdl->getConfig()[0]->header_text,
			'copyright'			=> $this->GeneralMdl->getConfig()[0]->copyright,
			'copyright_link'	=> $this->GeneralMdl->getConfig()[0]->copyright_link,
			'logout_link'		=> $this->GeneralMdl->getConfig()[0]->logout_link,
			// =============================================================================================
			'title'			=> 'Halaman Konfig Admin | AGT CODE', // TITLE BAR PADA BROWSER
			'h1'			=> 'Konfigurasi', // TEXT H1 UNTUK MENERANGKAN HALAMAN
			'small'			=> 'Halaman Konfigurasi Admin', // TEXT SMALL SETELAH H1
			'skin'			=> 'yellow', // SKIN ADMINLTE
			'nav'			=> $this->GeneralMdl->getMenu($this->usergroup), // DEFAULT (MODEL UNTUK MENGAMBIL MENU)
			'breadcrumb'	=> [
								'icon'	=> 'fa fa-cog', // IKON YANG AKAN DITAMPILKAN DI BREADCRUMB
								'bc'	=> ['Home','Konfigurasi Aplikasi'] // LIST BREADCRUMB
								]
		];
		return $data;
	}

	public function index(){
		$this->view_data['page_content'] = [
			'path' 	=> 'admin/config', // SESUAIKAN PATH TEMPLATE (BODY)
			'data'	=> 	[
							'data_config'	=> $this->GeneralMdl->getConfig()[0]
						]
		];
		$this->load->view('admin/template/template_view', $this->view_data);
	}

	public function save(){
		$config = array(
		        array(
		                'field' => 'header_text',
		                'label' => 'Nama Aplikasi',
		                'rules' => 'trim|required|alpha_numeric_spaces'
		        ),
		        array(
		                'field' => 'copyright',
		                'label' => 'Copyright',
		                'rules' => 'trim|required|alpha_numeric_spaces'
		        ),
		        array(
		                'field' => 'copyright_link',
		                'label' => 'Copyright Link',
		                'rules' => 'trim|required|valid_url'
		        ),
		        array(
		                'field' => 'logout_link',
		                'label' => 'Logout Link',
		                'rules' => 'trim|required|regex_match[/^[A-Za-z\/]+$/]'
		        ),
		        array(
		                'field' => 'captcha_time',
		                'label' => 'Sesi Captcha',
		                'rules' => 'trim|required|integer'
		        ),
		        array(
		                'field' => 'captcha_char',
		                'label' => 'Karakter Captcha',
		                'rules' => 'trim|required|integer|less_than[12]'
		        )
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
		 	$this->session->set_flashdata('status', validation_errors());
			redirect('admin/config');
		} else {
			$update = $this->ConfigMdl->updateConfig([
					'header_text'		=> $this->input->post('header_text',TRUE),
					'copyright'			=> $this->input->post('copyright',TRUE),
					'copyright_link'	=> $this->input->post('copyright_link',TRUE),
					'logout_link'		=> $this->input->post('logout_link',TRUE),
					'captcha_time'		=> $this->input->post('captcha_time',TRUE),
					'captcha_char'		=> $this->input->post('captcha_char',TRUE)
			]);

			if ($update) {
				$this->session->set_flashdata('status', 'Konfigurasi Berhasil Disimpan');
				redirect('admin/config');
			}
			else{
				$this->session->set_flashdata('status', 'Konfigurasi Gagal Disimpan');
				redirect('admin/config');
			}
		}
	}

	public function update_password(){
		$config = array(
		        array(
		                'field' => 'password_lama',
		                'label' => 'Password Lama',
		                'rules' => 'trim|required'
		        ),
		        array(
		                'field' => 'password_baru',
		                'label' => 'Password Baru',
		                'rules' => 'trim|required|matches[password_baru_c]'
		        ),
		        array(
		                'field' => 'password_baru_c',
		                'label' => 'Konfirmasi Password Baru',
		                'rules' => 'trim|required'
		        )
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
		 	$this->session->set_flashdata('status', validation_errors());
			redirect('admin/config');
		} else {
			$password = [
				'lama'	=> $this->input->post('password_lama',TRUE),
				'baru'	=> $this->input->post('password_baru',TRUE)
			];
			$updatePw = $this->ConfigMdl->updatePassword(['id_user' => $this->session->id_user],$password);
			if ($updatePw) {
				$this->session->set_flashdata('status', 'Passsword Berhasil Diperbarui');
				redirect('admin/config');
			}
			else{
				$this->session->set_flashdata('status', 'Passsword Gagal Diperbarui');
				redirect('admin/config');
			}
		}
	}
}