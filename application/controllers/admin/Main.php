<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	private $userid,$usergroup,$view_data;
	public function __construct(){
		parent::__construct();

		$this->userid 		= $this->session->id_user; // USER ID
		$this->usergroup 	= 1; // TENTUKAN USER GROUP UNTUK CONTROLLER INI

		/*----------------------- CONFIG DEFAULT (JANGAN DIHAPUS)-----------------------*/
		$this->load->model('AuthMdl'); 			// MODEL DEFAULT YANG SELALU DI LOAD (UNTUK OTENTIKASI)
		$this->AuthMdl->auth($this->usergroup); // VALIDASI USER YANG LOGIN
		$this->load->model('GeneralMdl'); 		// MODEL DEFAULT YANG SELALU DI LOAD
		$this->view_data = $this->config_page();
		/*-----------------------------------------------------------------------------*/
		
		$this->load->model('admin/ConfigMdl'); 	// MODEL KHUSUS CONTROLLER INI
	}

	private function config_page(){
		$data = [
			// =============================================================================================
			'app_name'			=> $this->GeneralMdl->getConfig()[0]->header_text,
			'copyright'			=> $this->GeneralMdl->getConfig()[0]->copyright,
			'copyright_link'	=> $this->GeneralMdl->getConfig()[0]->copyright_link,
			'logout_link'		=> $this->GeneralMdl->getConfig()[0]->logout_link,
			// =============================================================================================
			'title'			=> 'Halaman Dashboard Admin | Aplikasi Standar Kode', // TITLE BAR PADA BROWSER
			'h1'			=> 'Dashboard', // TEXT H1 UNTUK MENERANGKAN HALAMAN
			'small'			=> 'Halaman Dashboard Admin', // TEXT SMALL SETELAH H1
			'skin'			=> 'yellow', // SKIN ADMINLTE
			'nav'			=> $this->GeneralMdl->getMenu($this->usergroup), // DEFAULT (MODEL UNTUK MENGAMBIL MENU)
			'breadcrumb'	=> [
								'icon'	=> 'fa fa-dashboard', // IKON YANG AKAN DITAMPILKAN DI BREADCRUMB
								'bc'	=> ['Home','Dashboard'] // LIST BREADCRUMB
								]
		];
		return $data;
	}

	public function index(){
		$this->view_data['page_content'] = [
			'path' 	=> 'admin/main', // SESUAIKAN PATH TEMPLATE (BODY)
			'data'	=> 	[
							// DIDALAM ARRAY DATA ADALAH DATA YANG AKAN DI OPER KEDALAM
							// BODY TEMPLATE
							'greetings'	=> '<h3>Selamat Datang. Silahkan Pilih Menu Pada Samping Kiri Untuk Bekerja, Terima Kasih</h3>'
						]
		];
		$this->load->view('admin/template/template_view', $this->view_data);
	}

}