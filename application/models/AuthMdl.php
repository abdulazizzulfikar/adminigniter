<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthMdl extends CI_Model {

	public function auth($params){
		$usergroup = $this->session->id_group;
		if ($params != $usergroup) {
			$this->session->sess_destroy();
			redirect('Login');
		}
	}

	public function checkCredentials($params){
		$query = $this->db->select('user.id_user,user.username,user.password,user.id_group,user_group.redirect')
				 ->from('user')
				 ->join('user_group','user.id_group=user_group.id_group')
				 ->where('user.username',$params['user'])
				 ->or_where('user.email',$params['user'])
				 ->or_where('user.no_handphone',$params['user'])
				 ->get();
		if ($query->num_rows() > 0) {
			$data = $query->result();
			if (password_verify($params['password'],$data[0]->password) == TRUE){
				$sesi = [
					'username'	=> $data[0]->username,
					'id_group'	=> $data[0]->id_group,
					'id_user'	=> $data[0]->id_user
				];
				$this->session->set_userdata($sesi);
				redirect($data[0]->redirect);
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

}

/* End of file LoginMdl.php */
/* Location: ./application/models/LoginMdl.php */