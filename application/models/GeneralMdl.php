<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralMdl extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}

	public function getMenu($group_id){
		$query = $this->db->select('*')
				 ->from('user_menu')
				 ->where('id_group',$group_id)
				 ->order_by('order_menu','ASC')
				 ->get();
		$menuArr 	= [];
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				if($row->parent == NULL || $row->parent == '' ){
					$datasubMenu =	$this->db->select('nama_menu,link,icon')
									->from('user_menu')
									->where('parent',$row->id_menu)
									->get()
									->result();
					$menuArr[] = 
					[
						'nama_menu' 	=> $row->nama_menu,
						'url' 			=> $row->link,
						'icon'			=> $row->icon,
						'sub'			=> (!empty($datasubMenu) ? $datasubMenu : NULL)
					];
				}
			}
		}
		return $menuArr;
	}

	public function selectData($field,$table){
		$select	= 	$this->db->select($field)
					->from($table)
					->get();
		if($select->num_rows() > 0){
			return $select->result();
		}
		else{
			return FALSE;
		}
	}

	public function insertData($table,$data){
		$insert = $this->db->insert($table, $data);
		if($insert){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function updateData($data){
		$update	= 	$this->db->where($data['clause'])
					->update($data['table'],$data['form_data']);
		if($update){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function deleteData($table,$data){
		$delete	= 	$this->db->delete($table,$data);
		if($delete){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function selectWhere($data){
		$check	= 	$this->db->select($data['field'])
					->from($data['table'])
					->where($data['clause'])
					->get();
		if($check->num_rows() > 0){
			return $check->result();
		}
		else{
			return FALSE;
		}
	}

	public function searchData($data){
		$check	= 	$this->db->select($data['field'])
					->from($data['table'])
					->like($data['clause'])
					->get();
		if($check->num_rows() > 0){
			return $check->result();
		}
		else{
			return FALSE;
		}
	}

	public function countAll($data){
		$check	= 	$this->db->select($data['field'])
					->from($data['table'])
					->count_all_results();
		if($check > 0){
			return $check;
		}
		else{
			return 0;
		}
	}

	public function querySQL($query,$bind){
		$check	= 	$this->db->query($query,$bind);
		if($check){
			return $check;
		}
		else{
			return FALSE;
		}
	}
	
	public function getConfig(){
		$config_data = $this->selectWhere([
			'field'		=> '*',
			'table'		=> 'config',
			'clause'	=> ['id' => 1]
		]);
		if ($config_data) {
			return $config_data;
		}
		else{
			return FALSE;
		}
	}
}