<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigMdl extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->model('GeneralMdl');
	}

	public function updateConfig($data){
		return $this->GeneralMdl->updateData([
				'table'		=> 'config',
				'clause'	=> ['id' => 1],
				'form_data' => $data
		]);
	}	

	public function updatePassword($params,$password){
		$getUser = $this->GeneralMdl->selectWhere([
			'field'		=> 'id_user,password',
			'table'		=> 'user',
			'clause'	=> $params
		]);

		if (password_verify($password['lama'],$getUser[0]->password)) {
			$update = $this->GeneralMdl->updateData([
				'table'		=> 'user',
				'clause'	=> [key($params) => $params[key($params)]],
				'form_data'	=> ['password'	=> password_hash($password['baru'],PASSWORD_DEFAULT)]
			]);
			if ($update) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
}