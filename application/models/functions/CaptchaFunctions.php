<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CaptchaFunctions extends CI_Model {

	private $captcha_expired;
	public function __construct(){
		parent::__construct();
		$this->load->model('GeneralMdl');
		$this->captcha_expired = ($this->GeneralMdl->getConfig()[0]->captcha_time * 60);
	}

	public function captchaValidation($ip,$captcha){
		$query 	= 'SELECT ip_address,word FROM captcha WHERE ip_address = ? AND word = ?';
		$bind 	= [$ip,$captcha];
		$act 	= $this->GeneralMdl->querySQL($query,$bind);
		if(!$act->result()){
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	public function dbCaptchaManagement(){
		$expiration 	= time() - $this->captcha_expired;
		$sel_captcha	= [
			'field'			=> 'captcha_id,captcha_time,filename',
			'table'			=> 'captcha',
			'clause'		=> ['captcha_time < ' => $expiration]
		];

		$data_captcha 	= [
			'captcha_time'	=> $this->data_captcha['time'],
			'ip_address'	=> $this->data_captcha['ip_address'],
			'word'			=> $this->data_captcha['word'],
			'filename' 		=> $this->data_captcha['filename']
		];
		
		$check_captcha_exp = $this->GeneralMdl->selectWhere($sel_captcha);
		if ($check_captcha_exp) {
			foreach ($check_captcha_exp as $row) {
				$this->GeneralMdl->deleteData('captcha', ['captcha_id' => $row->captcha_id]);
			}
		}
		if ($this->GeneralMdl->insertData('captcha',$data_captcha)) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function captcha(){
		$config = array(
            'img_url' 		=> base_url() . 'assets/img/captcha/',
            'img_path' 		=> 'assets/img/captcha/',
            'img_height' 	=> 40,
            'img_width'		=> '145',
            'word_length' 	=> $this->GeneralMdl->getConfig()[0]->captcha_char,
            'expiration' 	=> $this->captcha_expired,
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 20, 80),
                'grid' => array(146, 202, 248)
        )
        );
        $captcha = create_captcha($config);
        return $captcha;
	}

}

/* End of file CaptchaFunctions.php */
/* Location: ./application/models/functions/CaptchaFunctions.php */