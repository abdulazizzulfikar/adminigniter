<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				Form Konfigurasi
			</div>
			<div class="panel-body">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 well well-lg">
					<legend>Konfigurasi Aplikasi</legend>
					<form action="<?= base_url('admin/config/save') ?>" method="POST" role="form">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
						<div class="form-group">
							<label>Nama Aplikasi</label>
							<input type="text" class="form-control" name="header_text" value="<?= $data_config->header_text ?>" autocomplete="off">
							<small class="text-danger">Ditampilkan Di Atas Side Menu</small>
						</div>
						<div class="form-group">
							<label>Copyright Text</label>
							<div class="input-group">
								<div class="input-group-addon">&copy;</div>
								<input type="text" class="form-control" name="copyright" value="<?= htmlentities($data_config->copyright)  ?>" autocomplete="off">
							</div>
							<small class="text-danger">Ditampilkan Di Footer</small>
						</div>
						<div class="form-group">
							<label>Copyright Link</label>
							<input type="text" class="form-control" name="copyright_link" value="<?= $data_config->copyright_link ?>" autocomplete="off">
							<small class="text-danger">Link Untuk Copyright</small>
						</div>
						<div class="form-group">
							<label>Logout Link</label>
							<div class="input-group">
								<div class="input-group-addon">base_url(</div>
								<input type="text" class="form-control" name="logout_link" value="<?= $data_config->logout_link ?>" autocomplete="off">
								<div class="input-group-addon">)</div>
							</div>
							<small class="text-danger">Link Untuk Logout Aplikasi, isi dengan lokasi dan nama controller / function, contoh login/logout</small>
						</div>
						<div class="form-group">
							<label>Sesi Captcha</label>
							<div class="input-group">
								<input type="text" class="form-control" name="captcha_time" value="<?= $data_config->captcha_time ?>" autocomplete="off">
								<div class="input-group-addon"><i class="fa fa-clock-o"></i> Menit</div>
							</div>
							<small class="text-danger">Sesi Captcha Akan Terhapus Dalam Menit</small>
						</div>
						<div class="form-group">
							<label>Karakter Captcha</label>
							<input type="text" class="form-control" name="captcha_char" value="<?= $data_config->captcha_char ?>" autocomplete="off">
							<small class="text-danger">Total Karakter Yang Ditampilkan Dalam Captcha (Maksimal 12)</small>
						</div>
						<button type="submit" class="btn btn-primary">Simpan Konfigurasi <i class="fa fa-cog"></i></button>
					</form>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 well-lg">
					<legend>Konfigurasi Password</legend>
					<form action="<?= base_url('admin/config/update_password') ?>" method="POST" role="form">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
						<div class="form-group">
							<label>Password Lama</label>
							<input type="password" class="form-control" name="password_lama">
							<small class="text-danger">Masukkan Password Lama</small>
						</div>
						<div class="form-group">
							<label>Password Baru</label>
							<input type="password" class="form-control" name="password_baru">
							<small class="text-danger">Masukkan Password Baru</small>
						</div>
						<div class="form-group">
							<label>Ketik Ulang Password Baru</label>
							<input type="password" class="form-control" name="password_baru_c">
							<small class="text-danger">Masukkan Lagi Password Baru</small>
						</div>
						<button type="submit" class="btn btn-primary">Ganti Password <i class="fa fa-lock"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				Status Penyimpanan Data
			</div>
			<div class="panel-body">
				<?php if ($this->session->flashdata('status')): ?>
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong><?= $this->session->flashdata('status'); ?></strong>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>