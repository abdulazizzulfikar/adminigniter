
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
  <style>
    .content-wrapper{
      background-image: url('<?= base_url() ?>assets/img/main/bg-content.png');
      background-repeat: repeat;
    }
  </style>
</head>
<body class="hold-transition skin-<?= $skin ?> sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
      <a href="javascript:void(0);" class="logo">
        <span class="logo-mini">
          <?php 
            preg_match_all("/(\S)\S*/i",$app_name,$initial,PREG_PATTERN_ORDER);
            foreach($initial[1] as $row){echo strtoupper($row);}
          ?>
        </span>
        <span class="logo-lg">
          <!-- dinamis, bisa dirubah sesuai role -->
          <?= strtoupper($app_name) ?>
        </span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar">
          <ul class="sidebar-menu" data-widget="tree">
              <li class="header"><i class="fa fa-list"></i> NAVIGASI MENU UTAMA</li>
              <?php $link = $this->uri->segment(1).'/'.$this->uri->segment(2); ?>
              <?php foreach ($nav as $row): ?>
                <?php if ($row['sub'] == NULL): ?>
                  <li class="<?php if(strtolower($link) == strtolower($row['url'])){echo'active';} ?>">
                    <a href="<?= base_url($row['url']); ?>">
                      <i class="fa <?= $row['icon'] ?>"></i>
                      <span><?= $row['nama_menu'] ?></span>
                    </a>
                  </li>
                <?php else: ?>
                  <li class="treeview">
                    <a href="javascript:void(0);">
                      <i class="fa <?= $row['icon'] ?>"></i>
                      <span><?= $row['nama_menu'] ?></span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <?php foreach ($row['sub'] as $submenu): ?>
                        <li class="<?php if(strtolower($link) == strtolower($row['url'])){echo'active';} ?>">
                          <a href="<?= base_url($submenu->link); ?>">
                            <i class="fa <?= $submenu->icon ?>"></i>
                            <span><?= $submenu->nama_menu ?></span>
                          </a>
                        </li>
                      <?php endforeach ?>
                    </ul>
                  </li>
                <?php endif ?>
              <?php endforeach ?>
              <li>
                <a href="<?= base_url($logout_link) ?>">
                  <i class="fa fa-sign-out"></i>
                  <span>Log Out</span>
                </a>
              </li>
          </ul>
      </section>
    </aside>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          <?= $h1 ?>
          <small class="text-primary"><?= $small ?></small>
        </h1>
        <ol class="breadcrumb">
          <?php foreach ($breadcrumb['bc'] as $key => $value): ?>
            <?php if ($key == 0): ?>
              <li><i class="<?= $breadcrumb['icon'] ?>"></i> <?= $value ?></li>
            <?php else: ?>
              <li><?= $value ?></li>
            <?php endif ?>
          <?php endforeach ?>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <div class="container-fluid">
             <?php $this->load->view($page_content['path'],$page_content['data']); ?>
          </div>
        </div>
      </section>
    </div>
    <footer class="main-footer">
      <strong><a href="<?= $copyright_link ?>" target="blank">&copy; <?= $copyright ?></a>.</strong>
    </footer>
  </div>
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/adminlte.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/datepicker.id.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>
