<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Halaman Login</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/AdminLTE.min.css">
  <style>
  	.bg-login{
  		background: #2193b0;
  		background: -webkit-linear-gradient(left, #6dd5ed, #2193b0);
  		background: -o-linear-gradient(left, #6dd5ed, #2193b0);
  		background: linear-gradient(to right, #6dd5ed, #2193b0);
  	}
  </style>
</head>
<body class="hold-transition bg-login">
<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0);" style="color: #FFF !important;">Login Page</a>
  </div>
  <div class="login-box-body">
  	<?php if ($this->session->flashdata('status')): ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong><?= $this->session->flashdata('status'); ?></strong>
		</div>
	<?php endif ?>
    <p class="login-box-msg">Silahkan Login Untuk Masuk Ke Sistem</p>
    <form action="<?= base_url('login/authorize') ?>" method="post">
      <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username / Email / No Handphone" autocomplete="no">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="no">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6">
            <input type="text" class="form-control" name="captcha" placeholder="Masukan Kode Captcha" autocomplete="no">
          </div>
          <div class="col-sm-6">
            <?= $captcha_img ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-block">Masuk</button>
        </div>
      </div>
    </form>
  </div>
</div>

<!-- jQuery 3 -->
<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script>
  $('img').addClass('img-responsive');
</script>
</body>
</html>
