# ADMIN IGNITER

Admin LTE panel, Login based role system, standard crud code

## Instalasi
Clone repository 

```bash
git clone https://abdulazizzulfikar@bitbucket.org/abdulazizzulfikar/adminigniter.git
```
Setup Config
```bash
1. Import Tabel adminigniter_db
2. Masuk ke url localhost/admingiter dan masukkan user admin dan password admin
```

## Fitur
1. Multiple login dengan multiple kondisi (menggunakan username,email dan nomor handphone)
2. Captcha Login
3. Menu Dinamis (icon, urutan menu dan submenu)
4. composser ready (applications/vendor)
5. Layout Dinamis (title bar, heading, breadcrumb, theme color)
6. AdminLTE yang sudah di kustom dan di pecah
7. General Query (CRUD,Select Where, Select Like, Count, SQL Query)
8. Support Library E-Chart, JQuery Chain, Sweet Alert, Air Datepicker, Fake Loader
9. Custom JS file (assets/js/custom.js)
10. Sudah Include Library Bahasa Indonesia
11. Custom 404 Page
12. Custom Error Page

## Dokumentasi

General Query insertData();
```php
$data = [
'nama'	=> 'aziz',
'umur'	=> 23
];
$this->GeneralMdl->insertData('namatabel',$data);
```

General Query updateData();
```php
$data = [
	'clause'	=> ['nama' => 'aziz'],
	'table'		=> 'namatabel',
	'form_data' => [
					'umur' => 22
				   ]
];
$this->GeneralMdl->updateData($data);
```

General Query deleteData();
```php
$param = ['umur' => 23];
$this->GeneralMdl->deleteData('namatabel',$param);
```

General Query selectData();
```php
$field = 'nama,umur,alamat'; // ATAU BISA DIISI * UNTUK SELECT ALL
$this->GeneralMdl->selectData($field,'namatabel');
```

General Query selectWhere();
```php
$data = [
	'field'		=> 'nama,umur,alamat',
	'table'		=> 'namatabel',
	'clause'	=> ['nama' => 'aziz']
];
$this->GeneralMdl->selectWhere($data);
```

General Query searchData();
```php
$data = [
	'field'		=> 'nama,umur,alamat',
	'table'		=> 'namatabel',
	'clause'	=> ['nama' => 'aziz']
];
$this->GeneralMdl->searchData($data);
```

General Query countAll();
```php
$data = [
	'field'		=> 'id',
	'table'		=> 'namatabel'
];
$this->GeneralMdl->countAll($data);
```

General Query querySQL();
```php
$query 	= "SELECT * FROM namatabel WHERE nama = ?";
$bind 	= ['aziz'];
$this->GeneralMdl->querySQL($query,$bind);
```


## Library Eksternal
1. [echart](https://echarts.apache.org/)
2. [sweet alert](https://sweetalert.js.org/guides/)
3. [jquery chain](https://appelsiini.net/projects/chained/)
4. [air datepicker](http://t1m0n.name/air-datepicker/docs/)
5. [fake loader](http://joaopereirawd.github.io/fakeLoader.js/)


## License
Open Source